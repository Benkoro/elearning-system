SET foreign_key_checks = 0;
DROP TABLE user CASCADE;
DROP TABLE course CASCADE;
DROP TABLE user_in_course CASCADE;

CREATE TABLE user (
     user_id MEDIUMINT NOT NULL AUTO_INCREMENT,
     name CHAR(60) NOT NULL,
     password CHAR(60) NOT NULL,
     PRIMARY KEY (user_id)
);

CREATE TABLE course (
     course_id MEDIUMINT NOT NULL AUTO_INCREMENT,
     name CHAR(60) NOT NULL,
     PRIMARY KEY (course_id)
);

CREATE TABLE user_in_course (
     user_id MEDIUMINT NOT NULL,
     course_id MEDIUMINT NOT NULL,
     state MEDIUMINT NOT NULL,
     PRIMARY KEY (user_id,course_id),
     CONSTRAINT FK_userId FOREIGN KEY (user_id) REFERENCES user(user_id),
     CONSTRAINT FK_courseId FOREIGN KEY (course_id) REFERENCES course(course_id)
);



insert into user (name, password) values ("root", "root");
insert into user (name, password) values ("student", "student");

insert into course (name) values ("backTraining");

insert into user_in_course (user_id, course_id, state) values (2, 1, 0);