package com.example.elearning.study;

import com.example.elearning.controllers.StudyController;
import com.example.elearning.course.Course;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(
        name = "ExamServlet",
        urlPatterns = {"/exam"}
)

public class ExamServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(ExamServlet.class);
//    private static final StudyController studyController = new StudyController();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        forwardExamPage(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        forwardExamPage(req,resp);
    }

    private void forwardExamPage(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String nextJSP = "/jsp/exams/" + req.getParameter("examName") + ".jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        dispatcher.forward(req, resp);
    }
}
