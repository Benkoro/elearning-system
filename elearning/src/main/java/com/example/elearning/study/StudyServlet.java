package com.example.elearning.study;

import com.example.elearning.ConnectionHelper;
import com.example.elearning.controllers.StudyController;
import com.example.elearning.course.Course;
import com.example.elearning.home.HomeServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

@WebServlet(
        name = "StudyServlet",
        urlPatterns = {"/study"}
)

public class StudyServlet extends HttpServlet {
    private static final Logger logger = LoggerFactory.getLogger(StudyServlet.class);
    private static final StudyController studyController = new StudyController();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String idUser;
        String courseName;
        String action = req.getParameter("action");
        if (action == null) {
            forwardStudyPage(req,resp);
        } else if (action.equals("usersCourses")){
            idUser = req.getParameter("idUser");
            Course course = studyController.getStudentCourses(Integer.parseInt(idUser));
            if (course != null) {
                resp.sendRedirect("jsp/courses/" + course.getName() + ".jsp");
            } else {
                forwardStudyPage(req,resp);
            }
        } else if (action.equals("assignCourse")) {
            idUser = req.getParameter("userId");
            courseName = req.getParameter("courseName");

            studyController.assignCourseToUser(courseName, Integer.parseInt(idUser));
            forwardStudyPage(req,resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        forwardStudyPage(req,resp);
    }

    private void forwardStudyPage(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String nextJSP = "/study.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        dispatcher.forward(req, resp);
    }
}
