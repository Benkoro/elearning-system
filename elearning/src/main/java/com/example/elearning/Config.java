package com.example.elearning;

public class Config {
    public static final String DB_HOST = "localhost";

    public static final int DB_PORT = 3306;

    public static final String DB_NAME = "elearning";

    public static final String DB_USER = "root";

    public static final String DB_PASSWORD = "root";
}
