package com.example.elearning.models;

import com.example.elearning.ConnectionHelper;
import com.example.elearning.user.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class UserModel {

    public void insertUser(User user) throws Exception {
        String query = "insert into user (name, password) values (?, ?)";

        try (Connection conn = ConnectionHelper.getConnection();
             PreparedStatement stmt = conn.prepareStatement(query)
        ) {
            stmt.setObject(1, user.getEmail());
            stmt.setObject(2, user.getPassword());
            stmt.execute();

            conn.close();
        }
    }

    public int selectUser(String email, String pass) throws Exception {
        String query = "SELECT * FROM user where name=\""+email+"\" and password=\""+pass+"\"";
        int userId = -1;

        try (Connection conn = ConnectionHelper.getConnection();
             PreparedStatement stmt = conn.prepareStatement(query)
        ) {
            ResultSet result = stmt.executeQuery();

            // iterate through the java resultset
            while (result.next())
            {
                userId = result.getInt("user_id");
            }

            conn.close();
        }
        catch (Exception e)
        {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }

        return userId;
    }
}
