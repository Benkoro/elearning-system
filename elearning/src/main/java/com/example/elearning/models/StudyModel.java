package com.example.elearning.models;

import com.example.elearning.ConnectionHelper;
import com.example.elearning.course.Course;
import com.example.elearning.course.State;
import com.example.elearning.user.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StudyModel {

    public Course selectCourses(int userId) throws Exception {
        String query = "SELECT name,state FROM course inner join user_in_course on user_in_course.course_id = course.course_id where user_in_course.user_id=" + userId;
        Course course = null;

        try (Connection conn = ConnectionHelper.getConnection();
             PreparedStatement stmt = conn.prepareStatement(query)
        ) {
            ResultSet result = stmt.executeQuery();

            String courseName = null;
            int state = -1;

            // iterate through the java resultset
            while (result.next())
            {
                courseName = result.getString("name");
                state = result.getInt("state");
            }

            course = new Course(courseName, state);
            conn.close();
        }
        catch (Exception e)
        {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }

        return course;
    }

    public int selectCourse(String courseName) throws Exception {
        String query = "SELECT course_id FROM course where name=\"" + courseName + "\"";
        int courseId = -1;

        try (Connection conn = ConnectionHelper.getConnection();
             PreparedStatement stmt = conn.prepareStatement(query)
        ) {
            ResultSet result = stmt.executeQuery();

            // iterate through the java resultset
            while (result.next())
            {
                courseId = result.getInt("course_id");
            }

            conn.close();
        }
        catch (Exception e)
        {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
        }

        return courseId;
    }

    public void addUserInCourse(int courseId, int userId, State state) {
        String query = "insert into user_in_course (user_id, course_id, state) values (?, ?, ?)";

        try (Connection conn = ConnectionHelper.getConnection();
             PreparedStatement stmt = conn.prepareStatement(query)
        ) {
            stmt.setObject(1, userId);
            stmt.setObject(2, courseId);
            stmt.setObject(3, 0);
            stmt.execute();

            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
