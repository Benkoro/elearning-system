package com.example.elearning.user;

import com.example.elearning.controllers.UserController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(
        name = "ProfileServlet",
        urlPatterns = {"/profile"}
)

public class ProfileServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(ProfileServlet.class);
    private static final UserController userController = new UserController();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        forwardProfilePage(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(req.getParameter("email"));
        System.out.println(req.getParameter("pass"));

//        try {
//            ConnectionHelper.prepareDatabase("root", "root");
//            logger.info("Connection successful");
//
//        } catch (SQLException e) {
//            logger.error("Connection not successful.", e);
//        } catch (NamingException e) {
//            logger.error("Connection not successful.", e);
//        }
//
//        if (userController.addNewUser(req)) {
//            forwardSignupOKPage(req,resp);
//        }
    }

    private void forwardProfilePage(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String nextJSP = "/profile.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        dispatcher.forward(req, resp);
    }
}
