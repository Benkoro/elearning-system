/* Copyright © 2015 Oracle and/or its affiliates. All rights reserved. */
package com.example.elearning.user;

import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author Kristian Benko
 */
public class User {

    private long id;
    private String name;
    private String lastName;
    private String password;
    private String birthDate;
    private String email;
    private Gender gender;
    private Education education;
    private static final AtomicLong counter = new AtomicLong(100);

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User(int userId, String email, String password) {
        this.id = userId;
        this.email = email;
        this.password = password;
    }

    public User(String name, String lastName, String password, String birthDate, Gender gender, Education education, String email, long id) {
        this.name = name;
        this.lastName = lastName;
        this.password = password;
        this.birthDate = birthDate;
        this.gender = gender;
        this.education = education;
        this.email = email;
        this.id = id;
    }

    public User(String name, String lastName, String password, String birthDate, Gender gender, Education education, String email) {
        this.name = name;
        this.lastName = lastName;
        this.password = password;
        this.birthDate = birthDate;
        this.gender = gender;
        this.education = education;
        this.email = email;     
        this.id = counter.incrementAndGet();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Education getEducation() {
        return education;
    }

    public void setEducation(Education education) {
        this.education = education;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name +
                ", lastName=" + lastName + ", birthDate=" + birthDate + 
                ", gender=" + gender + ", Education=" + education +
                ", email=" + email + ", password=" + password + '}';
    }

    
}
