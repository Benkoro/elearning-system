package com.example.elearning.user;

import com.example.elearning.ConnectionHelper;
import com.example.elearning.controllers.UserController;
import com.example.elearning.home.HomeServlet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(
        name = "SignupServlet",
        urlPatterns = {"/signup"}
)

public class SignupServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(SignupServlet.class);
    private static final UserController userController = new UserController();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        forwardSignupPage(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            ConnectionHelper.prepareDatabase("root", "root");
            logger.info("Connection successful");

        } catch (SQLException e) {
            logger.error("Connection not successful.", e);
        } catch (NamingException e) {
            logger.error("Connection not successful.", e);
        }

        if (userController.addNewUser(req)) {
            resp.sendRedirect("jsp/signup/signup-ok.jsp");
        }
    }

    private void forwardSignupPage(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String nextJSP = "/signup.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        dispatcher.forward(req, resp);
    }
}
