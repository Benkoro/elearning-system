package com.example.elearning.user;

import com.example.elearning.ConnectionHelper;
import com.example.elearning.SessionManager;
import com.example.elearning.controllers.UserController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.naming.NamingException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet(
        name = "LoginServlet",
        urlPatterns = {"/login"}
)

public class LoginServlet extends HttpServlet {

    private static final Logger logger = LoggerFactory.getLogger(LoginServlet.class);
    private static final UserController userController = new UserController();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        forwardLoginPage(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            ConnectionHelper.prepareDatabase("root", "root");
            logger.info("Connection successful");

        } catch (SQLException e) {
            logger.error("Connection not successful.", e);
        } catch (NamingException e) {
            logger.error("Connection not successful.", e);
        }

        User user = userController.getLoggedInUser(req.getParameter("email"), req.getParameter("pass"));//new User(req.getParameter("email"), req.getParameter("pass"));
        SessionManager.createSession(req,user);

        HttpSession sess = SessionManager.getSession();
        forwardLoggedInPage(req,resp);


//
//        if (userController.addNewUser(req)) {
//            forwardSignupOKPage(req,resp);
//        }
        //resp.sendRedirect("/home.jsp");
    }

    private void forwardLoginPage(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String nextJSP = "/login.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        dispatcher.forward(req, resp);
    }

    private void forwardLoggedInPage(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String nextJSP = "/jsp/login/userLogged.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        dispatcher.forward(req, resp);
    }
}
