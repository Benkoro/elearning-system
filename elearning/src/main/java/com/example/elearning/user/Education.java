package com.example.elearning.user;

public enum Education {
    NONE,
    ELEMENTARY,
    HIGHSCHOOL,
    UNIVERSITY
}
