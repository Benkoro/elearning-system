package com.example.elearning;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.naming.NamingException;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * Database connection helper containing singleton method
 * for connection, getting and disconnecting from the DB
 */
public class ConnectionHelper {

    /**
     * Pooled connection
     */
    private static MysqlDataSource dataSource;

    /**
     * Prepare database and connect with username and password
     *
     * @param username database username
     * @param password username database password
     * @throws SQLException oracle sql exception
     */
    public static void prepareDatabase(String username, String password)
            throws SQLException, NamingException {
        String dbUrl = getConnectionUrl();

        dataSource = new MysqlDataSource();
        dataSource.setURL(dbUrl);
        dataSource.setUser(username);
        dataSource.setPassword(password);
        dataSource.setServerName("myDBHost.example.org");

        // Connect
        dataSource.getConnection().close();
    }

    /**
     * Get prepared database connection
     * <p>
     * try (PooledConnection con = ConnectionHelper.getConnection();
     * PreparedStatement ps = createPreparedStatement(con, userId);
     * ResultSet rs = ps.executeQuery()) { ...
     *
     * @return prepared database connection
     * @throws SQLException has to be handled inside try-resource-catch
     */

    public static Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    /**
     * Prepare connection url
     *
     * @return connection url
     */
    private static String getConnectionUrl() {
        return "jdbc:mysql://" +
                Config.DB_HOST + ":" +
                Config.DB_PORT + "/" +
                Config.DB_NAME;
    }
}
