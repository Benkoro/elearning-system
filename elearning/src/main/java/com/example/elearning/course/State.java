package com.example.elearning.course;

public enum State {
    NOT_STARTED,
    DONE
}
