/* Copyright © 2015 Oracle and/or its affiliates. All rights reserved. */
package com.example.elearning.course;

import com.example.elearning.user.Education;
import com.example.elearning.user.Gender;

import java.util.concurrent.atomic.AtomicLong;

/**
 *
 * @author Kristian Benko
 */
public class Course {

    private long id;
    private String name;
    private State state;
    private static final AtomicLong counter = new AtomicLong(100);

    public Course(String name, int state) {
        this.name = name;
        this.state = State.values()[state];
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
