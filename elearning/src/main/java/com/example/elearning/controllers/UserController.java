package com.example.elearning.controllers;

import com.example.elearning.models.UserModel;
import com.example.elearning.user.User;

import javax.servlet.http.HttpServletRequest;

public class UserController {

    private UserModel userModel;

    public UserController() {
        userModel = new UserModel();
    }

    public boolean addNewUser(HttpServletRequest request) {
        String email = request.getParameter("email");
        String pass = request.getParameter("pass");

        User newUser = new User(email, pass);

        try {
            userModel.insertUser(newUser);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public User getLoggedInUser(String email, String pass) {
        User user = null;
        try {
            int userId = userModel.selectUser(email,pass);
            user = new User(userId, email, pass);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return user;
    }
}
