package com.example.elearning.controllers;

import com.example.elearning.course.Course;
import com.example.elearning.course.State;
import com.example.elearning.models.StudyModel;
import com.example.elearning.models.UserModel;
import com.example.elearning.user.User;

import javax.servlet.http.HttpServletRequest;
import java.sql.ResultSet;

public class StudyController {

    private StudyModel studyModel;

    public StudyController() {
        studyModel = new StudyModel();
    }

    public Course getStudentCourses(int userId) {
        try {
            return studyModel.selectCourses(userId);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void assignCourseToUser(String courseName, int userId) {
        int courseId = 0;
        try {
            courseId = studyModel.selectCourse(courseName);
        } catch (Exception e) {
            e.printStackTrace();
        }

        studyModel.addUserInCourse(courseId, userId, State.NOT_STARTED);
    }
}
