package com.example.elearning;

import com.example.elearning.user.User;
import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.sql.SQLException;

/**
 * SessionManager for user after logging in
 */
public class SessionManager {

    private static HttpSession session = null;

    /**
     * Create session with user and id
     *
     * @param user logged in user
     * @code id session's id
     */
    public static void createSession(HttpServletRequest req, User user) {
        session = req.getSession(true);
        session.setAttribute("username", user.getEmail());
        session.setAttribute("userId", user.getId());
    }

    public static HttpSession getSession() {
        return session;
    }

    public static void invalidate() {
        session.invalidate();
    }

    public static boolean isValid() {
        if (session != null){
            try {
                session.getAttribute("username");
            } catch (IllegalStateException e) {
                return false;
            }
        } else {
            return false;
        }

        return true;
    }
}
