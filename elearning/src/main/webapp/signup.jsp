<%@ page import="com.example.elearning.SessionManager" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>StudyWorkOut</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/signup.css" />
</head>
<body>
<div class="main">
    <div class="header">
        <div class="header_resize">
            <div class="menu_nav">
                <ul>
                    <li><a href="/home"><span>Home Page</span></a></li>
                    <li><a href="/study"><span>Study</span></a></li>
                    <li><a href="/statistics"><span>Statistics</span></a></li>
                    <li><a href="jsp/feedback/feedback.jsp"><span>Feedback</span></a></li>
                    <li class="dropdown active">
                        <%if (SessionManager.isValid()) {
                            out.println("<a><span>Welcome " + SessionManager.getSession().getAttribute("username") + "</span></a>");
                        } else {
                            out.println("<a><span>User</span></a>");
                        }%>
                        <div class="dropdown-content">
                            <%if (SessionManager.isValid()) {
                                out.println("<a href=\"/profile\">Profile</a>");
                                out.println("<a href=\"/study?idUser="+ SessionManager.getSession().getAttribute("userId")+"&action=usersCourses\">My courses</a>");
                                out.println("<a href=\"/jsp/login/userLoggedOut.jsp\">Logout</a>");
                            } else {
                                out.println("<a href=\"/login\">Login</a>");
                                out.println("<a href=\"/signup\">Sign up</a>");
                            }%>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="logo">
                <h1><a href="/home">StudyWork<span>Out</span></a></h1>
            </div>
            <div class="clr"></div>
        </div>
    </div>
    <!-- multistep form -->
    <form action="/signup" method="post" role="form" id="msform">
        <!-- progressbar -->
        <ul id="progressbar">
            <li class="active">Account Setup</li>
            <li>Personal Details</li>
        </ul>
        <!-- fieldsets -->
        <input type="hidden" id="idUser" name="idUser" value="${user.id}">
        <fieldset>
            <h2 class="fs-title">Create your account</h2>
            <h3 class="fs-subtitle">This is step 1</h3>
            <input type="text" name="email" placeholder="Email" id="email" value="${user.email}" required="true"/>
            <input type="password" name="pass" placeholder="Password" id="pass" value="${user.password}" required="true"/>
            <input type="password" name="cpass" placeholder="Confirm Password" id="cpass" required="true"/>
            <label for="cpass" id="labelCheckPasswordMatch"></label>
            <input type="button" name="next" class="next action-button" value="Next" />
        </fieldset>
        <fieldset>
            <h2 class="fs-title">Further Details</h2>
            <h3 class="fs-subtitle">We will never sell it</h3>
            <input type="text" name="name" placeholder="First Name" id="name" value="${user.name}"/>
            <input type="text" name="lastName" placeholder="Last Name" id="lastName" value="${user.lastName}"/>
            <input type="text" name="birthDate" placeholder="Birth date DD/MM/YYYY" id="birthDate" value="${user.birthDate}"/>
            <select name="education">
                <option value="0">Education..</option>
                <option value="1">None</option>
                <option value="2">Elementary school</option>
                <option value="3">High school</option>
                <option value="4">University</option>
            </select>
            <select name="gender">
                <option value="0">Gender..</option>
                <option value="1">Male</option>
                <option value="2">Female</option>
            </select>
            <input type="button" name="previous" class="previous action-button" value="Previous" />
            <button type="submit" name="submit" class="submit action-button">Submit</button>
        </fieldset>
    </form>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
    <script type="text/javascript" src="js/signup.js"></script>

</div>
</body>
</html>
