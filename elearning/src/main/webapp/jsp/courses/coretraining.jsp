<%@ page import="com.example.elearning.SessionManager" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>StudyWorkOut</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../../css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../css/coin-slider.css" />
    <script type="text/javascript" src="../../js/cufon-yui.js"></script>
    <script type="text/javascript" src="../../js/cufon-georgia.js"></script>
    <script type="text/javascript" src="../../js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="../../js/script.js"></script>
    <script type="text/javascript" src="../../js/coin-slider.min.js"></script>
</head>
<body>
<div class="main">
    <div class="header">
        <div class="header_resize">
            <div class="menu_nav">
                <ul>
                    <li><a href="/home"><span>Home Page</span></a></li>
                    <li class="active"><a href="/study"><span>Study</span></a></li>
                    <li><a href="/statistics"><span>Statistics</span></a></li>
                    <li><a href="jsp/feedback/feedback.jsp"><span>Feedback</span></a></li>
                    <li class="dropdown">
                        <%if (SessionManager.isValid()) {
                            out.println("<a><span>Welcome " + SessionManager.getSession().getAttribute("username") + "</span></a>");
                        } else {
                            out.println("<a><span>User</span></a>");
                        }%>
                        <div class="dropdown-content">
                            <%if (SessionManager.isValid()) {
                                out.println("<a href=\"/profile\">Profile</a>");
                                out.println("<a href=\"/study?idUser="+ SessionManager.getSession().getAttribute("userId")+"&action=usersCourses\">My courses</a>");
                                out.println("<a href=\"/jsp/login/userLoggedOut.jsp\">Logout</a>");
                            } else {
                                out.println("<a href=\"/login\">Login</a>");
                                out.println("<a href=\"/signup\">Sign up</a>");
                            }%>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="logo">
                <h1><a href="/home">StudyWork<span>Out</span></a></h1>
            </div>
            <div class="clr"></div>
            <div class="slider">
                <div id="coin-slider"> <a href="#"><img src="../../images/slide1.jpg" width="960" height="360" alt="" /><span> Tusce nec iaculis risus hasellus nec sem sed tellus malesuada porttitor. Mauris scelerisque feugiat ante in vulputate. Nam sit amet ullamcorper tortor. Phasellus posuere facilisis cursus. Nunc est lorem, dictum at scelerisque sit amet, faucibus et est. Proin mattis ipsum quis arcu aliquam molestie.</span></a> <a href="#"><img src="../../images/slide2.jpg" width="960" height="360" alt="" /><span> Tusce nec iaculis risus hasellus nec sem sed tellus malesuada porttitor. Mauris scelerisque feugiat ante in vulputate. Nam sit amet ullamcorper tortor. Phasellus posuere facilisis cursus. Nunc est lorem, dictum at scelerisque sit amet, faucibus et est. Proin mattis ipsum quis arcu aliquam molestie.</span></a> <a href="#"><img src="../../images/slide3.jpg" width="960" height="360" alt="" /><span> Tusce nec iaculis risus hasellus nec sem sed tellus malesuada porttitor. Mauris scelerisque feugiat ante in vulputate. Nam sit amet ullamcorper tortor. Phasellus posuere facilisis cursus. Nunc est lorem, dictum at scelerisque sit amet, faucibus et est. Proin mattis ipsum quis arcu aliquam molestie.</span></a> </div>
                <div class="clr"></div>
            </div>
            <div class="clr"></div>
        </div>
    </div>
    <div class="content">
        <div class="content_resize">
            <div class="mainbar">
                <h1><span>Core traning - course</span></h1>
                <div class="clr"></div>

                <div class="article">
                    <h2><span>Plank, plank, plank!</span></h2>
                    <p class="infopost">Posted <span class="date">on 11 sep 2018</span> by <a href="#">Admin</a> &nbsp;&nbsp;|&nbsp;&nbsp; Filed under <a href="#">group1</a></p>
                    <div class="clr"></div>
                    <div class="img"><img src="../../images/article2.jpg" width="178" height="185" alt="" class="fl" /></div>
                    <div class="post_content">
                        <p>THE PLANK - WHERE YOU GET into a pushup position, rest on your forearms, and hold for time is a fantastic core exercise. But as you seem to have found out, it doesn’t take long to max out the benefits. If you can hold a plank for two minutes or more, you need to up the intensity. The key is to progress very slowly. People see pictures and videos of very advanced versions that look fun, and then when they try them in their own workouts they fall flat on their faces. Don’t be those people. </p>
                        <p class="spec"><a href="#" class="rm">Read more</a> <a href="#" class="com">Comments <span>11</span></a></p>
                    </div>
                    <div class="clr"></div>
                </div>
            </div>

            <div class="clr"></div>
        </div>
    </div>
    <div class="footer">
        <div class="footer_resize">
            <p class="lf">Copyright &copy; <a href="#">Domain Name</a>. All Rights Reserved</p>
            <p class="rf">Design by <a target="_blank" href="http://www.dreamtemplate.com/">DreamTemplate</a></p>
            <div style="clear:both;"></div>
        </div>
    </div>
</div>
</body>
</html>
