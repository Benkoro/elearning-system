<%@ page import="com.example.elearning.user.User" %>
<%@ page import="com.example.elearning.SessionManager" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>StudyWorkOut</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../../css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../css/signup.css" />
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script type="text/javascript" src="../../js/experienceAPI/tincan.js"></script>
    <%--<script type="text/javascript" src="../../js/experienceAPI/signup-xapi.js"></script>--%>
</head>
<body>
<div class="main">
    <div class="header">
        <div class="header_resize">
            <div class="menu_nav">
                <ul>
                    <li><a href="/home"><span>Home Page</span></a></li>
                    <li><a href="/study"><span>Study</span></a></li>
                    <li><a href="/statistics"><span>Statistics</span></a></li>
                    <li><a href="jsp/feedback/feedback.jsp"><span>Feedback</span></a></li>
                    <li class="dropdown">
                        <a><span>User</span></a>
                        <div class="dropdown-content">
                            <a href=\"/login\">Login</a>
                            <a href=\"/signup\">Sign up</a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="logo">
                <h1><a href="/home">StudyWork<span>Out</span></a></h1>
            </div>
            <div class="clr"></div>
        </div>
    </div>
    <div style="position:absolute; width:100%; height:100%">
        <%
            SessionManager.invalidate();
        %>
        <img align="center" src="../../images/regcheck.png" style="margin-left: auto;margin-right: auto;display: block;width:100px;height:100px;">
        <h2 align="center">Now you are logged out!</h2>
    </div>
</div>
</body>
</html>
