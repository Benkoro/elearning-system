<%@ page import="com.example.elearning.SessionManager" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>StudyWorkOut</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="../../css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="../../css/coin-slider.css" />
    <script type="text/javascript" src="../../js/cufon-yui.js"></script>
    <script type="text/javascript" src="../../js/cufon-georgia.js"></script>
    <script type="text/javascript" src="../../js/jquery-1.4.2.min.js"></script>
    <script type="text/javascript" src="../../js/script.js"></script>
    <script type="text/javascript" src="../../js/coin-slider.min.js"></script>
</head>
<body>
<div class="main">
    <div class="header">
        <div class="header_resize">
            <div class="menu_nav">
                <ul>
                    <li><a href="/home"><span>Home Page</span></a></li>
                    <li class="active"><a href="/study"><span>Study</span></a></li>
                    <li><a href="/statistics"><span>Statistics</span></a></li>
                    <li><a href="jsp/feedback/feedback.jsp"><span>Feedback</span></a></li>
                    <li class="dropdown">
                        <%if (SessionManager.isValid()) {
                            out.println("<a><span>Welcome " + SessionManager.getSession().getAttribute("username") + "</span></a>");
                        } else {
                            out.println("<a><span>User</span></a>");
                        }%>
                        <div class="dropdown-content">
                            <%if (SessionManager.isValid()) {
                                out.println("<a href=\"/profile\">Profile</a>");
                                out.println("<a href=\"/study?idUser="+ SessionManager.getSession().getAttribute("userId")+"&action=usersCourses\">My courses</a>");
                                out.println("<a href=\"/jsp/login/userLoggedOut.jsp\">Logout</a>");
                            } else {
                                out.println("<a href=\"/login\">Login</a>");
                                out.println("<a href=\"/signup\">Sign up</a>");
                            }%>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="logo">
                <h1><a href="/home">StudyWork<span>Out</span></a></h1>
            </div>
            <div class="clr"></div>
            <div class="slider">
                <div id="coin-slider"> <a href="#"><img src="../../images/slide1.jpg" width="960" height="360" alt="" /><span> Tusce nec iaculis risus hasellus nec sem sed tellus malesuada porttitor. Mauris scelerisque feugiat ante in vulputate. Nam sit amet ullamcorper tortor. Phasellus posuere facilisis cursus. Nunc est lorem, dictum at scelerisque sit amet, faucibus et est. Proin mattis ipsum quis arcu aliquam molestie.</span></a> <a href="#"><img src="../../images/slide2.jpg" width="960" height="360" alt="" /><span> Tusce nec iaculis risus hasellus nec sem sed tellus malesuada porttitor. Mauris scelerisque feugiat ante in vulputate. Nam sit amet ullamcorper tortor. Phasellus posuere facilisis cursus. Nunc est lorem, dictum at scelerisque sit amet, faucibus et est. Proin mattis ipsum quis arcu aliquam molestie.</span></a> <a href="#"><img src="../../images/slide3.jpg" width="960" height="360" alt="" /><span> Tusce nec iaculis risus hasellus nec sem sed tellus malesuada porttitor. Mauris scelerisque feugiat ante in vulputate. Nam sit amet ullamcorper tortor. Phasellus posuere facilisis cursus. Nunc est lorem, dictum at scelerisque sit amet, faucibus et est. Proin mattis ipsum quis arcu aliquam molestie.</span></a> </div>
                <div class="clr"></div>
            </div>
            <div class="clr"></div>
        </div>
    </div>
    <div class="content">
        <div class="content_resize">
            <div class="mainbar">
                <h1><span>Back traning - course</span></h1>
                <div class="clr"></div>
                <div class="article">
                    <h2>Train back properly!</h2>
                    <div class="clr"></div>
                    <p>Here&#8217;s a question. When you do back exercises like barbell, dumbbell or machine rows, or pull-ups, chin-ups and lat pull-downs, are you <em>actually</em> using your back muscles?</p><p>I mean, do you <em>actually</em> feel the muscles in your back doing the work on every rep of every set of every back exercise you do?</p><p>If so, good for you.</p><p>But if not, then you are one of the many people with a very common problem: <strong>You use too much biceps and too little back during back exercises.</strong></p>
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/OXvQe9payHw" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                    <h2>How Does It Happen?</h2><p>It may seem a little strange at first to think that an exercise for one muscle group is being done by another muscle group altogether, but when you understand the mechanics of most back exercises, you can see why it&#8217;s extremely possible (and common).</p><p>Back exercises are <strong>pulling</strong> exercises, meaning they involve a weight being pulled towards you in either a <a href="https://www.aworkoutroutine.com/movement-patterns/">horizontal</a> (think rows) or <a href="https://www.aworkoutroutine.com/movement-patterns/">vertical</a> (think lat pull-downs) movement plane.</p><p>And, the thing about pulling exercises is that the <strong>biceps will always be recruited secondarily</strong> to some degree. And what that means is, it&#8217;s virtually impossible for anyone to do any type of back &#8220;pulling&#8221; exercise like a row or a pull-up without using their biceps.</p><p>This is actually a good thing for your biceps, as a very large percentage of the muscle and strength they gain will come as a result of doing <a href="https://www.aworkoutroutine.com/compound-exercises-vs-isolation-exercises/">compound exercises</a> like rows and pull-ups/pull-downs.</p><p>The big problem however is when your biceps take over so much so that they end up doing most, if not ALL of the work. Because when that happens, it means your back is going untrained to some degree, possibly even completely.</p><h2>What Causes This?</h2><p>There&#8217;s a few different reasons why your biceps are doing more work than your back muscles during back exercises.</p><p>However, in my experience, the reason usually falls into one of the following categories:</p><ul><li>The weight you&#8217;re using is too heavy for you.</li><li>You just don&#8217;t know how to properly &#8220;pull&#8221; with your back instead of your arms.</li><li>A combination of both.</li></ul><p>Now let&#8217;s take a look at the solutions to these causes.</p><h2>The Weight Is Too Heavy For You</h2><p>Walk into any typical gym in the world and you&#8217;ll ALWAYS see people doing various weight training exercises with a weight that is so obviously too heavy for them.</p><p>While I see it regularly from people of all genders, ages and body types, it&#8217;s usually more of a stupid guy thing. You know, lift more than you can handle to impress your friends, the girl on the bench or machine next to you, or just make your own ego feel good.</p><p>Whatever the reason, it&#8217;s beyond stupid for a ton of reasons. Here&#8217;s one: it&#8217;s preventing you from using the target muscle group(s).</p><p>Case in point&#8230; back exercises.</p><p>If you&#8217;re trying to do any sort of row or pull-up/lat pull-down with more weight than you can actually handle, then I can guarantee that you are using some weird jerky looking motion that is forcing your biceps (and God knows what else) to do most of the work.</p><p>And if you don&#8217;t think you&#8217;re using a weird jerky motion to get the weight where it needs to go, then I bet the weight just isn&#8217;t actually going where it needs to go.</p><p>Meaning, your range of motion sucks as a result of the weight being too damn heavy for you. You&#8217;re not going all the way up, all the way down, or a little of both.</p><p>In the case of most back exercises, you should be lowering the weight (or yourself in the case of pull-ups and chin-ups) until your elbows are fully extended. That means during an exercise like pull-ups, you should come to a dead hang in the bottom position. And during any sort of row, your arms should be fully extended straight out in front of you.</p><p>On the way up, you should be pulling the weight until its furthest position. In the case of rows, that&#8217;s until the weight is touching your chest or stomach and/or your elbows are behind your torso. In the case of pull-ups/pull-downs, your chin should be over the bar, and the bar should be touching (or coming within an inch or two of touching) the top of your chest.</p><p>I also want to add that the best test of all for ensuring that you&#8217;re using the right amount of weight is your ability to hold that end position for a second.</p><p>Meaning, when you reach the point at the end of each rep when you&#8217;ve pulled the weight (or yourself) to that top position, pause there for a second and squeeze your shoulder blades together.</p><p>If you can&#8217;t hold the end position for that 1 second squeeze, then you are most likely using a weight that is too heavy for you.</p><p>And as if it even needs to be said, if any of the above describes something you&#8217;re doing (or not doing) during back exercises, the simple solution is to just lower the amount of weight you&#8217;re using by however much you need to so that you can use perfect form and a full range of motion on every rep.</p><h2>You Just Don&#8217;t Know How To Use Your Back Muscles</h2><p>On the other hand, the weight you&#8217;re using might be perfectly fine for you. Instead, the problem might be that you just don&#8217;t know how to make your back muscles do the work instead of your biceps.</p><p>This is super common, and it mostly stems from the fact that your back is in &#8220;back&#8221; of you, and you can&#8217;t actually see it working or even picture it working like you can with say your chest or biceps.</p><p>And as most of us have realized at some point, it&#8217;s a little tricky to create a mind-muscle connection with a muscle you can&#8217;t actually see.</p><p>That is unless of course you know the tips and form cues that make activating your back muscles a lot easier. And they are&#8230;</p><ul class="listspace"><li><strong>Don&#8217;t try to pull the weight to you.</strong><br /> When you grab any type of bar or handle to do any form of row or pull-up/pull-down, our natural instinct is to try to pull that bar/handle into our body (or, in the case of pull-ups, our body up to the bar). Unfortunately, this thought of trying to pull the weight towards you is the first step in activating the biceps and eliminating the back. We need get rid of that thought. How? Like this&#8230;</li><li><strong>Don&#8217;t pull with your hands.</strong><br /> I know, that sounds crazy. You&#8217;re grabbing some sort of bar or handle with your hands, and I&#8217;m telling you not to use your hands to do any pulling during the exercise. Trust me, it&#8217;s going to make sense in a second. See, because we literally see our hands in front of us holding the bar, we naturally want to use them to pull the weight. When we do that, we use biceps, not back. We need to take our hands out of the equation. How? Like this&#8230;</li><li><strong>Think of your hands and forearms as hooks connecting your elbows to the weight.<br /> </strong>You need to imagine that your hands are doing nothing other than gripping the bar or handle. You want to avoid using them to pull the weight and instead only use them as &#8220;hooks&#8221; that connect your elbows to that weight. Why? Because&#8230;</li><li><strong>You must pull with your elbows!</strong><br /> And this right here is the key tip that everything else leads to making happen. Back exercises are all about the elbows. Instead of trying to pull the weight to you, and instead of using your hands to pull the weight, just grip the bar and then put all of your focus into moving your elbows back behind your body. Basically, imagine there is someone standing behind you that you want to hit with your elbows. Your goal is to try to hit them. And since your hands are &#8220;hooked&#8221; on to some type of weight, that weight will end up getting pulled towards you solely as a result of your elbows going back. It&#8217;s like a tow truck pulling a car. The weight is the car, your hands are the hooks, your forearms are the chains, and your elbows are the tow truck.And in the case of pull-ups and lat pull-downs, the only difference is that you&#8217;d be trying to move your elbows down to your sides rather than behind you like with rows. Either way, it&#8217;s all about gripping a weight with your hands, and then pulling through your elbows to drive that weight towards you. And when you reach that end point, squeeze your shoulder blades together for a second.</li><li><strong>An overhand or neutral grip may be more ideal than an underhand grip.</strong><br /> One final tip I want to mention is regarding the grip being used during back exercises. An underhand grip (where your palms are either facing the ceiling or you) puts your biceps in a stronger line of pull than either an overhand grip (palms facing the floor or away from you) or a neutral grip (palms facing each other). This means that an underhand grip potentially increases the amount of biceps involvement to some degree in comparison with the other grips. Don&#8217;t misunderstand me here&#8230; biceps will still definitely be recruited with an overhand and neutral grip as well. It&#8217;s just usually to a <em>slightly</em> higher degree with an underhand grip. So, when you&#8217;re working on perfecting the &#8220;pulling with your elbows&#8221; tip I mentioned before, you may be better off sticking with an overhand (or neutral) grip when performing back exercises.</li></ul><p>For some additional related advice, check out <a title="How To Build A Bigger Back: The 2 Best Back Training Tips" href="https://www.aworkoutroutine.com/how-to-build-a-bigger-back/">My 2 Best Tips For Building A Bigger Back</a>.</p>
                    <h2>The End</h2>
                </div>
            </div>
            <div class="sidebar">
                <div class="clr"></div>
            </div>
            <div class="clr"></div>
        </div>
    </div>
    <div class="footer">
        <div class="footer_resize">
            <p class="lf">Copyright &copy; <a href="#">Domain Name</a>. All Rights Reserved</p>
            <p class="rf">Design by <a target="_blank" href="http://www.dreamtemplate.com/">DreamTemplate</a></p>
            <div style="clear:both;"></div>
        </div>
    </div>
</div>
</body>
</html>
