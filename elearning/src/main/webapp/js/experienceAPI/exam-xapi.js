
var EXAM_ID = "http://adlnet.gov/EXAM_XAPI";
var QUESTIONS_ID = "http://adlnet.gov/QUESTIONS_ID_XAPI";
var tincan = new TinCan(
    {
        recordStores: [
            {
                endpoint: "http://147.95.40.129/data/xAPI/",
                username: "93498a1d4bcc3bb2bb3b87591e354394dd391f6a",
                password: "3d466044d5ad5e09d057f7c623bcff9365a2e213",
                allowFail: false
            }
        ]
    }
);
//"http://147.95.40.129/data/xAPI/",
//"93498a1d4bcc3bb2bb3b87591e354394dd391f6a",
//"3d466044d5ad5e09d057f7c623bcff9365a2e213",

// $(document).ready(function () {
//     //$('#tc_registrationSubmit').click(function () {
//         sendStatment_NewRegistration();
//     //});
// });


function sendStatment_ExamResult(result){
    var username = document.getElementById('idUser').value;

    var actorObj = {
        "name":username,
        "mbox":username + "@elearning.com"
    };
    var verbObj = {
        "id":"http://adlnet.gov/expapi/verbs/" + result,
        "display":{"en-US":result}
    };
    var registrationObj = {
        'id':EXAM_ID,
        "definition":{
            "type":"type:media",
            "name":{"en-US":"Examination - xAPI"}
        }
    };
    var contextObj = {
        "contextActivities":{
            "grouping":{"id":EXAM_ID}
        }
    };

    var stmt = {
        "actor": actorObj,
        "verb": verbObj,
        "object":registrationObj,
        "context": contextObj
    };

    stmtSender(stmt);
}

function sendStatment_QuestionResult(question, resultOfQuestion){
    var username = document.getElementById('idUser').value;

    var actorObj = {
        "name":username,
        "mbox":username + "@elearning.com"
    };
    var verbObj = {
        "id":"http://adlnet.gov/expapi/verbs/answered",
        "display":{"en-US":"answered"}
    };
    var registrationObj = {
        'id':QUESTIONS_ID + question,
        "definition":{
            "type":"type:media",
            "name":{"en-US":"Question "  + question}
        }
    };
    var contextObj = {
        "contextActivities":{
            "grouping":{"id":QUESTIONS_ID}
        }
    };
    var result = {
        "success": resultOfQuestion,
        "response": resultOfQuestion.toString()
    }

    var stmt = {
        "actor": actorObj,
        "verb": verbObj,
        "object":registrationObj,
        "context": contextObj,
        "result": result
    };

    stmtSender(stmt);
}

function stmtSender(stmt) {

    tincan.sendStatement(
        stmt,
        function (err, result) {
            //Handle any errors here. This code just outputs the result to the console.
            console.log(JSON.stringify(err, null, 4));
            console.log(JSON.stringify(result, null, 4));
        }
    )
}