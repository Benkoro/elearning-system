
var REGISTRATION_ID = "http://adlnet.gov/SIGNUP_XAPI";
var tincan = new TinCan(
    {
        recordStores: [
            {
                endpoint: "http://147.95.40.129/data/xAPI/",
                username: "93498a1d4bcc3bb2bb3b87591e354394dd391f6a",
                password: "3d466044d5ad5e09d057f7c623bcff9365a2e213",
                allowFail: false
            }
        ]
    }
);

$(document).ready(function () {
    //$('#tc_registrationSubmit').click(function () {
        sendStatment_NewRegistration();
    //});
});


function sendStatment_NewRegistration(){
    var actorObj = {
        "name":"New user",
        "mbox":"newuser@elearning.com"
    };
    var verbObj = {
        "id":"http://adlnet.gov/expapi/verbs/registrated",
        "display":{"en-US":"registrated"}
    };
    var registrationObj = {
        'id':REGISTRATION_ID,
        "definition":{
            "type":"type:media",
            "name":{"en-US":"New registration - xAPI"}
        }
    };
    var contextObj = {
        "contextActivities":{
            "grouping":{"id":REGISTRATION_ID}
        }
    };

    var stmt = {
        "actor": actorObj,
        "verb": verbObj,
        "object":registrationObj,
        "context": contextObj
    };

    stmtSender(stmt);
}

function stmtSender(stmt) {

    tincan.sendStatement(
        stmt,
        function (err, result) {
            //Handle any errors here. This code just outputs the result to the console.
            console.log(JSON.stringify(err, null, 4));
            console.log(JSON.stringify(result, null, 4));
        }
    )
}