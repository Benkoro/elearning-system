<%@ page import="com.example.elearning.SessionManager" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>StudyWorkOut</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="css/signup.css" />
</head>
<body>
<div class="main">
    <div class="header">
        <div class="header_resize">
            <div class="menu_nav">
                <ul>
                    <li><a href="/home"><span>Home Page</span></a></li>
                    <li><a href="/study"><span>Study</span></a></li>
                    <li><a href="/statistics"><span>Statistics</span></a></li>
                    <li><a href="jsp/feedback/feedback.jsp"><span>Feedback</span></a></li>
                    <li class="dropdown active">
                        <%if (SessionManager.isValid()) {
                            out.println("<a><span>Welcome " + SessionManager.getSession().getAttribute("username") + "</span></a>");
                        } else {
                            out.println("<a><span>User</span></a>");
                        }%>
                        <div class="dropdown-content">
                            <%if (SessionManager.isValid()) {
                                out.println("<a href=\"/profile\">Profile</a>");
                                out.println("<a href=\"/study?idUser="+ SessionManager.getSession().getAttribute("userId")+"&action=usersCourses\">My courses</a>");
                                out.println("<a href=\"/jsp/login/userLoggedOut.jsp\">Logout</a>");
                            } else {
                                out.println("<a href=\"/login\">Login</a>");
                                out.println("<a href=\"/signup\">Sign up</a>");
                            }%>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="logo">
                <h1><a href="/home">StudyWork<span>Out</span></a></h1>
            </div>
            <div class="clr"></div>
        </div>
    </div>
    <!-- multistep form -->
    <form action="/login" method="post" role="form" id="msform">
        <!-- fieldsets -->
        <input type="hidden" id="idUser" name="idUser" value="${user.id}">
        <fieldset>
            <h2 class="fs-title">Login</h2>
            <input type="text" name="email" placeholder="Email" id="email" value="${user.email}" required="true"/>
            <input type="password" name="pass" placeholder="Password" id="pass" value="${user.password}" required="true"/>
            <button type="submit" name="submit" class="submit action-button">Login</button>
        </fieldset>
    </form>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js'></script>
    <script type="text/javascript" src="js/signup.js"></script>

</div>
</body>
</html>
